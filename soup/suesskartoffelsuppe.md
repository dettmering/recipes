Süßkartoffelsuppe
=================

Zutaten
-------

* 2 #Knoblauchzehe
* 1 #Süßkartoffel
* 2 #Karotten
* 1 rote #Paprika
* 2 rote #Zwiebeln
* #Ingwer
* 750 ml Wasser
* #Gemüsebrühe (16.5 g bei Alnatura)
* #Olivenöl
* #Petersilie

Zubereitung
-----------

1. Süßkartoffeln, Karotten und Paprika würfeln, 2-3 cm3 Ingwer klein hacken. 
2. Zwiebeln klein hacken.
3. ca. 5 EL Olivenöl in Topf erhitzen. Knoblauch plattdrücken und so lange in Öl anschwitzen, bis die Zehen ganz leicht bräunlich werden. Zehen sofort entfernen.
4. Zwiebeln ein paar Minuten im Öl glasig anschwitzen
5. Restliches Gemüse hinzugeben und 5-10 min anschwitzen
6. Wasser und Gemüsebrühe hinzugeben, 20 min auf kleiner Flamme köcheln lassen
7. Mit Pürierstab zerkleinern, mit Pfeffer und Salz abschmecken
8. Servieren mit etwas Olivenöl und Petersilie