Gemüsesuppe V1
==============

Zutaten
-------

* #Knoblauchzehe
* #Suppengrün
* Rote Beete
* 1 getrocknete Chile Guajilla
* 1 l #Gemüsebrühe
* #Rapsöl

Zubereitung
-----------

1. Suppengrün und Rote Beete würfeln.
2. Gemüsebrühe vorbereiten.
3. Topfboden mit Rapsöl bedecken und auf voller Stufe heiß machen.
4. Knoblauchzehe zerdrücken und in den Topf geben. Wenn die Zehe bräunlich wird, aus dem Öl entfernen.
5. Gewürfeltes Gemüse andünsten.
6. Mit Gemüsebrühe auffüllen und Chilischote hinzugeben.
7. Auf niedrigster Stufe 30 min köcheln lassen.
8. Abschmecken und essen.

Es können auch Griesnockerln oder Markklößchen hinzugegeben werden.
