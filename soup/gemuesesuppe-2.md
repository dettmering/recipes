Gemüsesuppe V2
==============

Unterschied zu Gemüsesuppe V1: Statt roter Beete Ingwer und Hot Chili Sauce. Schmeckt besser.

Zutaten
-------

* #Knoblauchzehe
* #Suppengrün
* ca. 3 cm Stück #Ingwer
* 1 getrocknete Chile Guajilla
* ca. 1 ml Chile La Anita Hot Pepper Sauce
* 1 l #Gemüsebrühe
* Rapsöl

Zubereitung
-----------

1. Suppengrün würfeln. Ingwer schälen und klein schneiden.
2. Gemüsebrühe vorbereiten.
3. Topfboden mit Rapsöl bedecken und auf voller Stufe heiß machen.
4. Knoblauchzehe zerdrücken und in den Topf geben. Wenn die Zehe bräunlich wird, aus dem Öl entfernen.
5. Gewürfeltes Gemüse andünsten.
6. Mit Gemüsebrühe auffüllen und Chilischote hinzugeben.
7. Chile La Anita hinzugeben.
8. Auf niedrigster Stufe 30 min köcheln lassen.
9. Abschmecken und essen.

Es können auch Griesnockerln oder Markklößchen hinzugegeben werden.
