Pasta Salad a la Mom
====================

Ingredients
-----------

 -  500g Pasta, ideally short pasta
 -  500g #MiracleWhip
 -  500g #Joghurt
 -  2 #Paprika, different colors
 -  #Mais und #Erbsen aus Dose

Protocol
--------

1. Cook pasta
2. Cut Bell pepers into small pieces
3. Add peas and corn
4. Mix with yoghurt and Miracle Whip
5. Add pasta, peper and salt:
