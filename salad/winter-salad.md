Wintersalat
===========

Zutaten
-------

 -   250g #Rinderfilet
 -   250g Fusilli
 -   300g #Champignons
 -   200g #Feldsalat
 -   100g #Joghurt
 -   5 #Cocktailtomaten
 -   #Olivenöl
 -   Lemongrass infused Rapsöl (falls vorhanden)
 -   #Weißweinessig
 -   #Balsamico
 -   #Quittengelee
 -   #Pfeffer (aus Mühle), #Salz, Zucker

Zubereitung
-----------

Fusilli in kochendem Salzwasser 11 Minuten garen, danach abschrecken und abtropfen lassen. Champignons putzen und vierteln. Champignons in Olivenöl leicht anbraten, dann Nudeln untermischen und in Schüssel geben, abkühlen lassen.

Für das Dressing je nach Geschmack ca. 5 EL Olivenöl, 1 EL Lemongrass-Rapsöl, 1 TL Weißweinessig, 1 El Joghurt, 1 TL Quittengelee und 1/2 TL Zucker mischen.

Feldsalat putzen und waschen. Tomaten vierteln, Lauchzwiebeln hacken und in Olivenöl leicht anbraten, dabei darauf achten, dass Tomaten nicht matschig werden. Rinderfilet klein schneiden, beidseitig mit grobem Pfeffer und Salz würzen und in der Pfanne ein paar Minuten scharf anbraten.

Nudeln unter den Feldsalat heben, dann Rinderfilet, Tomaten, Lauchzwiebeln und Dressing untermischen. 
