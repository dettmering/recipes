Hirsesalat
==========

Zutaten
-------

 - 250 g #Hirse
 - 8 #Cocktailtomaten
 - 1 #Zwiebel rot
 - 1 Bund #Petersilie glatt
 - 1 Bund #Pfefferminz
 - 1 #Paprika rot
 - 1 #Gurke
 - 3 #Frühlingszwiebeln
 - #Tomatenmark
 - Saft 1/2 #Zitrone
 - heller #Balsamico
 - #Olivenöl
 - #Pfeffer, #Salz, #Paprikapulver

Zubereitung
-----------

1. Ziebeln würfeln und in Pfanne mit 1 EL Tomatenmark anbraten.
2. Hirse dazugeben und mit 2 Tassen Wasser ablöschen. 15 min kochen, mit Pfeffer und Salz würzen.
3. Gemüse kleinschneiden und mit Zitronensaft mischen, Hirse dazugeben.
4. 5 EL Olivenöl, 2 EL Balsamico mischen und dazugeben, mit Kräutern vermischen.
