Hackbraten mit Gemüse
=====================

Quelle: https://frechefreunde.de/rezepte-fuer-kinder/hackbraten-mit-gemuese/

Zutaten
-------

 - 1 große #Karotte
 - 1 #Zucchini
 - 1 Bund #Petersilie
 - 1 EL #Schnittlauch
 - 1 #Zwiebel
 - 100 g Semmelbrösel oder Haferflocken
 - 1 Ei
 - 1 EL #Tomatenmark
 - 400 g Bio-Rinderhackfleisch

Soße:

 - 250 g Kirschtomaten
 - 3 EL Olivenöl
 - 2 Knoblauchzehen
 - Salz & Pfeffer

Protokoll
---------

 1. Den Backofen auf 190 Grad Umluft vorheizen. Die Karotte und die Zucchini mit einer feinen Reibe raspeln. Die geraspelten Zucchini gut ausdrücken, um das überschüssige Wasser zu entfernen. Die Zwiebel schälen und in sehr feine Würfel schneiden. Die Petersilie hacken.
 2. Alle Zutaten gut miteinander vermengen. Die Masse dafür am besten mit den Händen kneten und anschließend zu einem Quader formen.
 3. Eine Pfanne mit etwas Olivenöl erhitzen. Den Hackbraten von allen Seiten scharf anbraten und anschließend in eine Auflauf- oder Backform geben. Mit dem Bratfett übergießen und in den Backofen schieben. Für etwa 45 Minuten im Backofen garen.
 4. Die Tomaten waschen und zusammen mit dem Knoblauch in eine Auflaufform geben. Mit etwa 3-4 EL Olivenöl beträufeln. Nach belieben salzen & pfeffern und für etwa 20 Minuten in den Backofen geben. Anschließend zu einer Sauce pürieren.
 5. Die Tomatennudeln nach Anleitung kochen und zusammen mit der Sauce und dem Hackbraten servieren.
