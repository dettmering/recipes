# Rinderrouladen

Modifiziert nach https://emmikochteinfach.de/klassische-rinderroulade/

## Zutaten

### Rouladen

- 6 Scheiben Rinderrouladen - 1 vorgeschnittene Roulade wiegt ca. 160-200g
- 6 Scheiben Speck - durchwachsen
- 6 Gewürzgurken - dicke, längs geteilt
- 2 TL Dijon Senf - pro Roulade
- 2 Schalotten; groß - klein geschnitten
- 1 EL Butterschmalz - zum scharf anbraten
- Salz und Pfeffer
- Küchengarn oder Zahnstocher

### Soße

- 1 Bund Suppengrün - je 1 Lauch, Möhre, Sellerie, klein geschnitten
- 2 Schalotten, groß - grob geschnitten
- 1 TL Tomatenmark
- 1 TL Zucker
- (400 ml Rotwein (trocken))
- 400 ml Rinderbrühe oder Fond
- 60 g Butter - eiskalt, in Würfel
- Salz und Pfeffer

## Protokoll

**Vorbereitung**:

1. Zutaten bereitstellen, einschließlich Gewürze für die Soße und Tomatenmark.
2. Rouladen mit einem Fleischklopfer oder Fleischplattierer auf eine Dicke von 0,5 bis 1 cm klopfen.

**Zubereitung der Rouladen**:

3. Rouladen auslegen und jeweils mit Salz, Pfeffer, 2 TL Dijonsenf, einer Speckscheibe, einer längs geschnittenen Gewürzgurke und kleingeschnittenen Schalotten belegen.
4. Rouladen-Seiten einklappen, aufrollen und das Ende nach unten klappen. Mit Küchengarn oder Zahnstocher sichern.

**Braten und Schmoren**:

5. Ofen auf 160 Grad Umluft vorheizen.
6. Butterschmalz in einem Bräter oder einer Schmorpfanne erhitzen. Rouladen scharf anbraten und herausnehmen.
7. Gemüse, Schalotten, Tomatenmark und Zucker in die Pfanne geben und ca. 5 Minuten andünsten. Rotwein in drei Teilen zugeben, jeweils einkochen lassen.
8. Rinderbrühe/Rinderfond hinzufügen. Qualität der Brühe beachten, Rinderfond aus dem Glas empfohlen.
9. Rouladen zurück in die Pfanne legen und im vorgeheizten Ofen für 90 Minuten garen. Optional zugedeckt.
10. Garzeit prüfen. Wenn notwendig, weitere 15-30 Minuten im Ofen lassen.

**Soße**:

11. Gemüse und Soße durch ein Sieb in einen Topf passieren. Soße 3-5 Minuten einkochen lassen. Vom Herd nehmen und eiskalte Butterwürfel unterrühren, bis die Soße gebunden ist.

**Servieren**:

12. Rouladen mit der Soße servieren. Als Beilage eignen sich Kartoffeln, Nudeln oder Spätzle. Guten Appetit!