# Rindergulasch à la ChatGPT

## Zutaten (für ca. 3–4 Personen)
- **600 g Rindergulasch** (ca. 3x3 cm große Stücke, ggf. selbst schneiden)  
- **400 ml Rinderfond** (selbstgemacht oder aus dem Glas)  
- **300 g Schalotten** (geschält und halbiert)  
- **200 ml Granatapfelsaft** 
- **40 g Tomatenmark**  
- **3 EL Butterschmalz** (z. B. Buttaris; alternativ Pflanzenöl)  
- **1 Knoblauchzehe** (geschält, fein gewürfelt)  
- **1–2 TL Zitronenabrieb** (unbehandelt/Bio, optional)  
- **1 Lorbeerblatt**  
- **3 Pimentkörner** (oder 1/4 TL gemahlen)  
- **1/4 TL Kreuzkümmel (Cumin)**  
- **1 Prise Zimt** (optional)  
- **Schwarzer Pfeffer** (frisch gemahlen, nach Geschmack)  
- **1 Prise Salz**

## Zubereitungsschritte

### Vorbereitung
1. Das Fleisch ca. 30 Minuten vor der Zubereitung aus dem Kühlschrank nehmen, damit es Zimmertemperatur erreicht.  
2. Falls nötig, das Fleisch selbst in ca. 3x3 cm große Würfel schneiden.  
3. Das Fleisch nur mäßig salzen, höchstens mit einer Prise Salz kurz vor dem Anbraten würzen.  
4. Die Schalotten schälen und halbieren. Besonders große Schalotten ggf. vierteln.

### Anbraten
1. **Schalotten anbraten:** 1 EL Butterschmalz im Bräter erhitzen. Die Schalotten darin goldgelb anbraten, herausnehmen und beiseitestellen.
2. **Fleisch portionsweise anbraten:** 1 EL Butterschmalz erneut im Bräter erhitzen. Bei hoher Temperatur das Fleisch in kleinen Portionen anbraten, dabei erst nach ca. 1 Minute wenden, um eine gute Bräunung und Röstaromen zu erzielen. Die angebratenen Fleischstücke beiseitestellen. **Tipp:** Immer portionsweise arbeiten, damit der Boden die Hitze hält. Sonst wird das Fleisch eher gekocht als gebraten.

### Fleisch und Tomatenmark rösten
1. Das gesamte Fleisch zurück in den Bräter geben.  
2. Schalotten und 40 g Tomatenmark hinzufügen. Das Tomatenmark gut unterrühren und kurz mitrösten.

### Ablöschen und Würzen
1. Mit 200 ml Granatapfelsaft ablöschen. Den Saft auf etwa ein Drittel einkochen lassen.  
2. Währenddessen den Knoblauch fein würfeln. Die Zitrone heiß abwaschen, trocknen und die Schale fein abreiben (für ca. 1–2 TL Zitronenabrieb, optional).  
3. Sobald der Saft reduziert ist, Knoblauch, Zitronenabrieb, 1 Lorbeerblatt, Piment, Kurkuma, Kreuzkümmel, Zimt und schwarzen Pfeffer hinzufügen und kurz unterrühren.  

### Schmoren
1. Das Fleisch und die Schalotten mit 400 ml Rinderfond ablöschen.  
2. Mit Deckel bei geringer Hitze ca. 1,5 Stunden auf der Herdplatte schmoren lassen.  
3. Nach ca. 1 Stunde prüfen, ob die Soße zu flüssig ist. Falls ja, die letzten 30 Minuten ohne Deckel weitergaren.  
4. Am Ende der Garzeit das Gulasch nach Belieben abschmecken. Falls nötig, die Soße leicht eindicken.  
