Reitersuppe
===========

Modifiziert nach https://www.meinestube.de/reitersuppe/

Zutaten
-------

- 500 g #Rinderhackfleisch
- Olivenöl oder Rapsöl
- #Prinzessbohnen oder #Erbsen
- 3-4 #Karotten
- 1 große #Zwiebel
- 2 Zehen #Knoblauch
- 2 EL #Tomatenmark
- 1 Dose gestückelte Tomaten
- 1 rote #Paprika
- 500 ml #Gemüsebrühe
- 3-4 mittelgroße geschälte #Kartoffeln
- 3-4 Stängel #Thymian, bzw 1 TL
- #Salz, #Pfeffer und #Paprikapulver
- ggf. Creme Fraiche

Protokoll
---------

1. ggf. Gemüsebrühe zubereiten
2. Öl erhitzen, Zwiebel und Knoblauch andünsten, dann Hackfleisch darin anbraten
3. Salz, Pfeffer, Paprika, Tomatenmark, Zucker hinzugeben
4. Mit Brühe ablöschen, Dose Tomaten, Karotten, Bohnen/Erbsen, Thymian hinzugeben, gut verrühren
5. Kartoffeln klein schneiden und hinzugeben
6. ca. 45 min auf kleiner bis mittlerer Hitze köcheln lassen, mindestens bis Kartoffeln gar sind.
