# Kalbssauté

## Zutaten

- 750 g #Kalbschulter (oder auch #Rindfleisch)
- 2 #Zwiebeln
- 1/4 #Sellerie
- 200 ml #Rinderfond
- 2 #Karotten 
- 1 Glas #Ajuvar
- 1 #Paprika rot
- 1/2 TL #Thymian getrocknet

## Zubereitung

1. Fleisch abtrockenen und in gleichmäßige Würfel schneiden
2. Zwiebeln schälen und grob hacken. Karotten, Sellerie und Paprika putzen und in Stücke hacken
3. Öl erhitzen und Fleisch anbraten. Zwiebeln glasig anschwitzen, dann restliches Gemüse hinzugeben und kurz mit anbraten.
4. Rinderfond und 3 TL Ajuvar hinzugeben. Mit Salz, Pfeffer und Thymian würzen.
5. 40 Minuten garen lassen bis Fleisch mürbe ist