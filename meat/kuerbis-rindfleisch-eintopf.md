Kürbis-Rindfleisch-Eintopf
==========================

Abgewandelt nach https://www.brigitte.de/rezepte/kuerbis-rindfleisch-eintopf-11263476.html

Zutaten
-------

- 1 Bund #Thymian
- 1 Zweig #Rosmarin
- 1 #Lorbeerblatt
- 700 g #Rindfleisch
- Rapsöl zum Anbraten
- 2 EL #Tomatenmark
- 400 ml Kalbsfond
- 1 rote Zwiebeln
- 1/2 Hokkaidokürbis

Zubereitung
-----------

1. Fleisch abspülen, trocken tupfen und in grobe Stücke schneiden. Fleisch gut pfeffern und salzen, am besten über Nacht einlegen in Öl, Pfeffer, Salz, Thymian, Rosmarin, Knoblauch.
2. Den Backofen auf 130 Grad Ober-Unterhitze vorheizen. Zwiebel abziehen und fein würfeln. Kürbis in mundgerechte Stücke schneiden.
3. Öl in einem großen Bräter erhitzen, 2 ganze Knoblauchzehen zerdrücken und im Öl andünsten. Sobald sie leicht bräunlich werden, entfernen. Die Fleischstücke darin bei starker Hitze rund­ herum anbraten und salzen. Fleischstücke entfernen.
4. Tomatenmark dazugeben und etwa 1 Minute anrösten. Zwiebeln und Kürbis hinzugeben und kurz andünsten. Alle Fleischstücke in den Bräter geben. Fond dazugießen und unter Rühren aufkochen lassen. Kräuter hinzufügen
5. Bräter schließen, auf den Ofenrost stellen und auf der 2. Schiene von unten 3 Stunden langsam schmoren lassen. Gelegentlich umrühren.
6. Nach dem Garen den Topf offen auf den Herd stellen und etwas einkochen lassen.