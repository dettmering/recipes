# Spaghetti mit Meatballs

Für 3 Personen

## Zutaten

- 500 g Rinderhackfleisch
- 1 Bund Petersilie glatt
- Semmelbrösel
- 1 Ei
- 2 Zehen Knoblauch
- 1/2 Zwiebel
- 400 mL passierte Tomaten
- Rapsöl
- 1 Karotte
- Thymian getrocknet
- Pfeffer, Salz

## Zubereitung Fleischbällchen

1. Ofen auf 200 °C Umluft vorheizen
2. Hackfleisch, Ei, kleingehackte Petersilie, Pfeffer, Salz, 1/2 TL Thymian, 1/4 kleingehackte Zwiebel, 1 gepresste Zehe Knoblauch in eine Schüssel geben und verkneten. Das Hackfleisch sollte nicht mehr "nass" sein, ansonsten mehr Semmelbrösel hinzufügen.
3. Fleischbällchen rollen und in eine unbeschichtete Pfanne legen, die man in den Ofen stellen kann. Fleischbällchen mit Rapsöl betreichen und darin rollen. Pfanne in Ofen stellen.
4. 15-30 min braten lassen, bis die Fleischbällchen bräunlich sind.

## Zubereitung Tomatensoße

1. Karotten fein würfeln, Knoblauch fein hacken.
2. Restliche 1/4 Zwiebel, Knoblauch, Karotten 5 min in Rapsöl in einem kleinen Topf anschwitzen.
3. Passierte Tomaten hinzugeben, etwas Olivenöl dazu, Pfeffer und Salz
4. 10 Minuten kochen lassen
5. Pürieren

## Zusammenbau

1. Pfanne aus dem Ofen holen. Dabei sich möglichst nicht verbrennen. Topflappen auf dem Griff liegen lassen als Erinnerung.
2. 2-3 min abkühlen lassen, dann Tomatensoße hinzugeben, gut verrühren
3. Fleischbällchen in Tomatensoße köcheln lassen.
4. Spaghetti kochen
5. Spaghetti, Fleischbällchen und Soße vermengen und servieren mit Parmesan.