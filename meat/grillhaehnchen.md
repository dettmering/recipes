# Grillhähnchen

Rohes Hähnchen = Biohazard! Nicht abwaschen, Handschuhe tragen, keine Gegenstände kontaminieren. Am besten in einer großen Edelstahlschale arbeiten. Abfall und Handschuhe direkt entsorgen und Oberflächen nach der Arbeit desinfizieren. Geschirr direkt in der Spülmaschine mit 70 °C spülen.

## Zutaten

- 1 qualitativ hochwertiges Hähnchen, ca. 1,5 kg
- frischer Salbei
- frischer Thymian
- frischer Rosmarin
- Pfeffer, Salz
- 1 Zitrone mit verzehrbarer Schale

## Vorbereitung

Am Vorabend das Hähnchen marinieren:

1. Marinade herstellen: Die Hälfte der Kräuter kleinhacken und mit Rapsöl und einem Stück Butter vermengen. Geriebene Schale der Zitrone, Pfeffer und Salz hinzufügen
2. Das Hähnchen trocken tupfen
3. Mit einem Finger die Haut an allen Stellen vorsichtig ablösen
4. Marinade mit dem Finger gründlich unter der Haut verteilen, die komplette Marinade aufbrauchen
5. Restliche Kräuter und eine halbe Zitrone in die Körperhöhle stecken
6. Mit Frischhaltefolie abdecken, in eine große Schale legen und diese nochmals mit Frischhaltefolie abdecken
7. Hähnchen mit Küchengarn zubinden
8. Über Nacht im Kühlschrank marinieren lassen

## Zubereitung

1. 1 h vor Zubereitung aus dem Kühlschrank holen, Frischhaltefolie entfernen
2. Haut durchlöchern mit einem Zahnstocher
3. Haut mit grobem Salz gut bestreuen, damit die Feuchtigkeit entzogen wird
4. Ofen auf 220 °C Umluft vorheizen
5. Hähnchen auf eine Bratschale mit Metallrost legen
6. Salz und entzogene Flüssigkeit abtupfen.
7. ca. 1 h im Ofen braten, bis die dickste Stelle im Fleisch 74 °C Kerntemperatur erreicht hat
8. Aus dem Ofen holen und ca 20 min ziehen lassen, dann servieren