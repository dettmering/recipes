Chili con Carne
===============

Zutaten
-------

  - 400g gemischtes #Hackfleisch
  - 1 Dose #Kidneybohnen
  - 1 Dose #Mais
  - passierte #Tomaten
  - Tomaten gestückelt
  - ca. 1 ml Chile La Anita Hot Pepper Sauce
  - 1 getrocknete Chile Guajilla
  - 1 Stück dunkle #Schokolade
  - #Zwiebeln
  
Protokoll
---------

  1. Öl in einem Topf erhitzen (Boden soll bedeckt sein)
  2. Knoblauchzehe platt drücken und zum Öl geben
  3. Wenn die Zehe anfängt bräunlich zu werden, rausnehmen und wegwerfen
  4. Zwiebeln ins Öl geben, bis sie glasig werden
  5. Hackfleisch dazugeben und anbraten
  6. Bohnen, Tomaten, Mais, Chili, Schokolade hinzugeben
  7. 30 min köcheln lassen
  8. mit Salz und Pfeffer abschmecken