Hackfleisch Zucchini und möglicherweise Broccoli
================================================

Zutaten
-------

  - 300 g #Rinderhackfleisch
  - 1 rote #Zwiebel
  - 1 mittlere #Zucchini (20 cm x 5 cm)
  - möglicherweise #Broccoli nach belieben
  - 2 Zehen #Knoblauch
  - Pfeffer, Salz, #Thymian, Schnittlauch, gehackte Mandeln
  - #Joghurt als Topping

Protokoll
---------

  1. Zucchini und Zwiebeln fein hacken
  2. Olivenöl in Pfanne erhitzen, Zwiebeln und zerdrückte Knoblauchzehen anschwitzen
  3. Broccoli dünsten
  4. Zucchini in Pfanne geben und ebenfalls anschwitzen, pfeffern und salzen
  5. Gemüse auf eine Hälfte der Pfanne schieben, Hackfleisch auf anderer Hälfte anbraten
  6. Gemüse und Hackfleisch gut vermischen und mehrere Minuten anbraten
  7. Broccoli untermengen
  8. Mit Thymian, Schnittlauch und Mandeln bestreuen