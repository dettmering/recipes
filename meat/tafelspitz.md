# Tafelspitz

Modifiziert nach https://thomassixt.de/rezept/tafelspitz/

## Zutaten

- 700 g Tafelspitz (Hüfte bzw. Spitze des Schwanzstücks vom Rind oder Kalb )
- 1-2 #Zwiebel (Schalotten)
- 2-3 #Knoblauchzehe
- 1 Bund #Suppengemüse
- ca. 4 Liter Wasser
- 1 EL #Salz
- 1 TL #Pfefferkörner
- 1 TL #Kümmel
- 1 TL #Wacholderbeeren
- 1 TL #Majoran 
- 1 TL #Thymian 
- 4 Stück #Lorbeerblatt
- 1 Bund #Petersilie
- 2 Stück #Markknochen

## Zubereitung

1. Das Fleisch unter fließendem, kalten Wasser abwaschen und trocken tupfen.
2. Die Zwiebel halbieren und in einer Pfanne ohne Fett dunkel rösten.
3. Das Suppengemüse bestehend aus Karotten, Sellerie, Petersilienwurzeln und Lauch waschen. Das Wurzelgemüse schälen und in Stücke schneiden, den Lauch im ganzen belassen oder blättrig schneiden.
4. Vorbereiteten Tafelspitz in einen hohen Topf legen. Gemüse, Markknochen, geröstete Zwiebel, Knoblauch und Markknochen in den Topf legen. Mit Salz würzen.
5. Die Suppenzutaten mit kaltem Wasser auffüllen und aufkochen.
6. Die Suppe nach dem ersten aufkochen ca. 1,5 Stunden leicht siedend ziehen lassen. Den Schaum, der sich an der Oberfläche bildet erst später mit einer Schöpfkelle abnehmen.
7. Das Fleisch ist fertig, wenn sich ohne Widerstand mit einer Fleischgabel einstechen lässt.
8. Das gekochte Fleisch quer zur Faser in 1 cm große Stücke aufschneiden.
