Herbstliches Rindergulasch
==========================

Geht auch mit Lamm (Keule) anstatt Rind. Abgewandelt nach https://www.oetker.de/rezepte/r/herbstliches-lammgulasch.html

Zutaten
-------

- 250 g #Zwiebeln
- 250 g #Karotte 
- 1 Hokkaido #Kürbis
- 200 g #Porree (Lauch)
- 200 g gekochte Maronen (Esskastanien)
- 1 Zweig #Rosmarin
- 1 Zweig #Thymian
- 500 g Rindfleisch
- 2 EL Rapsöl
- 1 Zehe Knoblauch
- 1 geh. EL #Tomatenmark
- 375 ml trockener #Rotwein (Weißwein wenn's sein muss)
- 150 ml Wasser
- Salz
- frisch gemahlener Pfeffer
- 1 Becher Crème fraîche

Zubereitung
-----------
1. Zwiebeln abziehen und in Würfel schneiden. Möhren und Kartoffeln schälen, Porree putzen. Das Gemüse in etwas kleinere Würfel schneiden. Maronen halbieren. Kräuter hacken. Lammfleisch mit Küchenpapier trocken tupfen. Rind in knapp 2 cm große Würfel schneiden.
2. Öl in einem großen Topf erhitzen. Knoblauch zerdücken und dünsten, bis er bräunlich wird, dann entfernen. Fleisch hinzufügen und unter Wenden darin bräunen. Zwiebelwürfel dazugeben und kurz mitbräunen.
3. Übrige Gemüsewürfel, Maronen, Kräuter und Tomatenmark hinzugeben und ebenfalls kurz anbraten. Mit etwa 125 ml Wein ablöschen und diesen einkochen lassen. Danach wieder mit 125 ml Wein ablöschen und ebenfalls reduzieren lassen. Übrigen Wein, Wasser, Salz und Pfeffer hinzufügen. Alles zum Kochen bringen und zugedeckt etwa 35 Min. bei mittlerer Hitze köcheln lassen.
4. Das Gulasch nochmals abschmecken, Crème fraîche unterrühren und anrichten.