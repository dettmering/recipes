Reis
====

Zutaten
-------

 - 300 g #Reis (Langkorn oder Basmati)
 - 450 ml Wasser (1.5x Reismenge)
 - #Pfeffer, #Salz, 1 #Sternanis, 2 #Kardamom

Protokoll
---------

Immer mit kaltem Wasser arbeiten.

 1. 300 g Reis in einem feinen Sieb unter kaltem Wasser gut waschen und dann in einen Topf geben.
 2. Pfeffer, Salz, Sternanis und zerdrückten Kardamom hinzugeben
 3. 450 ml kaltes Wasser dazugeben
 4. Mit Deckel Wasser auf höchster Stufe zum Kochen bringen und dann sofort auf niedrigste Stufe stellen
 5. Reis mindestens 12 min köcheln lassen, NIE den Deckel öffnen, weil der Reis dampfgaren soll
 6. Deckel abnehmen, Kardamom und Sternanis entfernen und Reis durchmischen