Kürbisrisotto
=============

Quelle: https://frechefreunde.de/rezepte-fuer-kinder/freches-risotto-kuerbis/

Zutaten
-------

- halber Hokkaido-Kürbis
- 1 #Zucchini
- halbe #Zwiebel
- 300g Risotto #Reis
- 1 l #Gemüsebrühe
- Sonnenblumenöl, Salz, Peffer

Protokoll
---------

 1. Gemüsebrühe in einem Topf erhitzen und beiseitestellen.
 2. Zwiebel kleinschneiden und in etwas Sonnenblumenöl in einem großen Topf anschwitzen.
 3. Risotto Reis hinzugeben und für ein paar Minuten mit andünsten.
 4. Die beiseite gestellte Gemüsebrühe nach und nach zum Reis geben und bei mittlerer Hitze köcheln lassen, bis der Risotto Reis alle Flüssigkeit aufgesaugt hat und ausgequollen ist.
 5. Kürbis und Zucchini reiben (geht gut mit Küchenmaschine). Anschließend das klein geschnittene Gemüse mit etwas Öl in einer Pfanne anbraten und unter das fertige Risotto mischen. Zum Schluss noch mit etwas Salz und Pfeffer abschmecken.
