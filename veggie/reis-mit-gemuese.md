Reis mit Gemüse
===============

Zutaten
-------

 - 350 g #Reis
 - 2 #Paprika
 - 1 #Zucchini
 - 1 #Aubergine
 - 1 #Knoblauchzehe
 - Tahimipaste, #Sojasoße
 
Protokoll
---------

Nach Geschmack ca. 50 ml Sojasoße mit einer Messerspitze Tahimipaste vermengen.

 1. Reis kochen wie beschrieben
 2. Paprika, Aubergine, Zucchini sehr dünn schneiden
 3. Aubergine salzen und mit Olivenöl vermengen, 10 min ziehen lassen
 4. zerdrückte Knoblauchzehe in Olivenöl leicht andünsten und dann wegwerfen
 5. Aubergine dünsten
 6. Paprika und Zucchini dazugeben, nochmal Olivenöl dazu, 10 min mit Deckel kochen lassen
 7. Pfeffer, Salz und etwas Dressing dazugeben
 8. Reis unterheben und servieren