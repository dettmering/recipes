# Grüne Soße

Rezept aus dem Umschlag der Kräuter

## Zutaten

- 200g frische grüne Soße Kräuter (#Petersilie, #Sauerampfer, #Schnittlauch, #Kerbel, #Borretsch, #Kresse, #Pimpinelle)
- 200g #Joghurt
- 200g #Schmand
- 1 TL #Senf
- 4 hart gekochte #Eier
- Saft einer halben #Zitrone

## Zubereitung

1. 200g Schmand und 200g Joghurt mit 1 TL Senf vermengen. Die Masse mit Salz und weißem Pfeffer abschmecken.
2. Vier Eier koche, abschrecken, pellen und würfeln. Die gewürfelten Eier unter die Schmand-Joghurt Masse heben.
3. Die Kräuter waschen, hacken und zu der Masse hinzugeben. Gut verrühren.
4. Mit Pfeffer, Salz und Zitronensaft abschmecken.