# Melanzane alla Parmigiana

Modizifiert nach https://sz-magazin.sueddeutsche.de/das-rezept/melanzane-alla-parmigiana-auberginen-auflauf-89104

## Zutaten

- 2 Auberginen
- 400 g Tomaten passiert
- 1 Zwiebel
- 2 Knoblauchzehen
- 2 Päckchen Büffelmozzarella
- Parmesan
- frisches Basilikum

## Zubereitung

Backofen auf 180°C vorheizen.

### Auberginen

1. Auberginen der Länge nach in dünne Scheiben schneiden
2. Salzen und in ein Sieb legen, dort 30 min die Flüssigkeit entziehen lassen
3. Abwaschen und trocken tupfen
4. In einer heißen Pfanne mit Rapsöl anbraten und beiseite legen

### Tomatensoße

1. Knoblauch und Zwiebel hacken, in Ölivenöl anschwitzen
2. Tomatensoße hinzugeben, mit Pfeffer, Salz, Thymian und Olivenöl würzen
3. 10 min köcheln lassen

### Zusammenbau

1. In einer Auflaufform Tomatensoße, Aubergine, Büffelmozzarellascheiben schichten, dazwischen Parmesan reiben
2. Letzte Schicht Parmesan
3. 30 min bei 180°C in den Ofen