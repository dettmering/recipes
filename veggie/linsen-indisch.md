Linsen indische Art
===================

Zutaten
-------

 - 1 Bund #Lauchzwiebeln
 - 500 g #Broccoli 
 - 250 g #Kirschtomaten
 - 200 g rote #Linsen
 - 3 EL Olivenöl
 - ca. 700 ml #Gemüsebrühe
 - 1 Dose große weiße #Bohnen (240 g Abtropfgewicht)
 - fein abgeriebene Schale und Saft von ½ Bio-#Zitrone
 - Currygewürz

Zubereitung
-----------

1. Lauchzwiebeln putzen und in Ringe schneiden. Brokkoli putzen, in Röschen teilen. Strunk schälen, in Scheiben schneiden und in kochendem Salzwasser 1 Minute vorgaren. Brokkoliröschen zufügen und ca. 2 Minuten mit blanchieren. Auf einem Sieb abgießen, kalt abschrecken und abtropfen lassen. Tomaten waschen. Rote Linsen auf einem Sieb waschen und abtropfen lassen.
2. Die Hälfte der Zwiebelringe mit Linsen in 1 EL heißem Öl ca. 1 Minute unter Wenden anschwitzen. Dabei mit Garam Masala und Salz würzen. 600 ml Brühe angießen, aufkochen und halb zugedeckt ca. 15 Minuten weich garen. Bei Bedarf noch etwas Brühe angießen.
3. Inzwischen übrige Zwiebelringe mit Tomaten im restlichen heißen Öl 2-3 Minuten anbraten. Bohnen auf einem Sieb abgießen, abspülen und abtropfen lassen. Mit Brokkoli zu den Tomaten geben. Mit Salz, Pfeffer, Zitronenschale und 1 EL -saft würzen und 100 ml Brühe angießen. Zugedeckt 5-8 Minuten schmoren. Gemüse und Linsen-Dal abschmecken. Dal auf tiefe Teller verteilen, Gemüse darauf geben und servieren.