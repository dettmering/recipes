# Gegrillte Auberginen mit Zitronen-Beurre-blanc

## Zutaten

### Für die Beurre blanc
- 250 g Butter (kalt, gewürfelt)
- 1 Schalotte
- 1 Lorbeerblatt
- 10 Körner weißer Pfeffer
- 250 ml Weißwein (trocken)
- 1/2 Zitrone (Saft)
- Salz
- Pfeffer

### Für die Auberginen
- 2 Auberginen
- Olivenöl
- 1 Zwiebel
- 1 Handvoll Petersilie (frisch)
- 300 g Champignons
- 1 TL Knoblauchpaste
- 1 Handvoll Semmelbrösel
- Balsamico (alt)
- Salz
- Pfeffer

## Zubereitung

1. **Beurre blanc vorbereiten**  
   Butter in Würfel schneiden und kühl stellen. Schalotte fein hacken und mit Lorbeer, Pfefferkörnern und etwas Butter anschwitzen. Mit Weißwein ablöschen, einkochen lassen und durch ein Sieb geben. Zurück in den Topf geben, erhitzen und kalte Butterwürfel nach und nach einrühren. Mit Zitronensaft, Salz und Pfeffer abschmecken.

2. **Auberginen backen & grillen**  
   Auberginen längs halbieren, Fruchtfleisch gitterartig einschneiden, mit Olivenöl beträufeln und salzen. Mit der Schnittseite nach oben bei 180°C Umluft 30–40 Min. backen. Danach in einer Pfanne beidseitig goldbraun grillen.

3. **Pilz-Topping zubereiten**  
   Zwiebel und Petersilie fein hacken. Champignons in Scheiben schneiden und mit Zwiebeln in Olivenöl anschwitzen. Knoblauchpaste und Petersilie hinzufügen.

4. **Auberginen fertigstellen**  
   Gegrillte Auberginen mit der Pilzmischung bedecken, mit Semmelbröseln bestreuen und weitere 7–12 Min. im Ofen backen.

5. **Anrichten**  
   Auberginen mit Pfeffer würzen, mit Balsamico beträufeln und zusammen mit der Beurre blanc servieren.