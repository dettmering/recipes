Gebackene Aubergine
===================

Zutaten
-------

pro Portion

* 1 #Aubergine
* 5 #Cocktailtomaten
* 1 rote #Zwiebel
* 2 Knoblauchzehen
* Öl
* #Pfeffer und #Salz
* frisch: #Thymian, #Petersilie, #Pfefferminz 
* Mango-Granatapfel-Chutney

Zubereitung
-----------

1. Aubergine längs mehrfach anschneiden, sodass sie zusammen bleibt
2. Gut mit Salz bestreuen und 30 min ziehen lassen. Ofen auf 200 Grad vorheizen
3. In der Zwischenzeit Zwiebel kleinhacken, Tomaten vierteln, Knoblauchzehen pressen, Kräuter hacken, einen großen Teelöffel Chutney hinzugeben und mit Öl und etwas Pfeffer vermengen.
4. Aubergine gut abwaschen, trockentupfen und in eine Auflaufform legen.
5. Aubergine füllen und für 30 Minuten backen.