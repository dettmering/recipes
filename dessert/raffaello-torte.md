Raffaello Torte
===============

Zutaten Teig
------------

  - 50 g #Kokos (geraspelt)
  - 125 g #Zucker
  - 125 g #Butter
  - 4 #Eigelb, 4 #Eiweiß (getrennt)
  - 100 g #Mehl
  - 3 TL #Backpulver
  
Protokoll Teig
--------------

  1. Butter und Zucker verrühren
  2. Eigelb dazumischen
  3. Mehl, Backpulver und Kokos mischen und dazugeben
  4. Eiweiß steifschlagen und unter die Mischung heben
  5. In eine runde Backform geben, glatt streichen und alles bei 160°C Umluft 15 min backen.
  6. Auskühlen lassen
  
Zutaten Belag
-------------

  - 300 g Sahne
  - 20 g Kokosraspel
  - 1 Päckchen Sahnesteif
  - 2 Packungen Raffaello
  - 2 Schalen Erdbeeren
  
Protokoll Belag
---------------

  1. Flüssige Sahne und Sahnesteif steifschlagen und Raffaello-Stückchen (1 Packung/10-15 Stück, vorher gut zerkleinern) unterheben
  2. Auf den ausgekühlten Boden halbe Erdbeeren verteilen und die Raffaello-Sahne darüber streichen. In den Kühlschrank stellen.

Tipp: Zur Dekoration halbe Raffaello-Kugeln auf die Sahne setzen.
