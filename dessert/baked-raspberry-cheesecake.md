Baked Raspberry Cheesecake
==========================

after G. Ramsay ([Source](http://s-vick.blogspot.de/2013/04/gordon-ramsays-ultimate-cookery-course_11.html?m=1))

Ingredients
-----------

  - #Butter For Greasing
  - 550g cream cheese, at room temperature (sold as Philadephia in Germany), alternatively #Quark 30%
  - 160g Caster Sugar
  - 3 Eggs, lightly beaten
  - 2 tbsp Plain Flour
  - Finely grated zest of 1 Lemon, juice of half a lemon
  - 200g Raspberries
  - (1 Tbsp Schmand)
  - Try adding a splash of vanilla

Protocol
--------

  1. Preheat the oven to 180C and butter a 23 cm spring-foam cake tin.
  2. Beat together the cream cheese and sugar. Add the beaten eggs bit by bit until combined. Add the flour and lemon zest anf juice, then fold through the raspberries. 
  3. Spoon the mixture into the prepared cake tin, tapping it against the work surface to remove any bubbles and help the raspberries rise up from the bottom. Bake in the preheated oven for 35 minutes until set on the edges but wobbling slightly in the middle.
  4. Once cooked, remove from the tin, running knife around the edges to loosen if necessary, and serve.