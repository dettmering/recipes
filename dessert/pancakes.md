Pfannkuchen
===========

Modifiziert nach https://www.chefkoch.de/rezepte/1265881231835442/1-A-Eierpfannkuchen.html

Ingredients
-----------

 - 350 g #Mehl
 - 70 g #Zucker
 - 6 #Eier
 - 250 ml #Milch
 - 250 ml Mineralwasser, mit Kohlensäure
 - 1 TL, gestr. #Backpulver
 - Pflanzenöl, zum Backen

Protocol
--------

  1. Eier trennen. Eiweiß und Zucker steif schlagen. Eigelb mit Milch und Wasser verrühren. Mehl mit Backpulver mischen und unter das Eigelb-/Milch-/Wassergemisch rühren. Eischnee unterheben.
  2. Fett in der Pfanne erhitzen, die Pfannkuchen bei mittlerer Hitze backen, bis die Unterseite goldbraun ist, die Pfannkuchen drehen, und wie oben noch mal backen.
  3. Die Pfannkuchen werden durch den Eischnee, das Mineralwasser und das Backpulver sehr luftig und locker.
