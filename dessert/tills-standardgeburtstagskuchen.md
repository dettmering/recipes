Tills Standardgeburtstagskuchen
===============================

Zutaten
-------

  - 2-3 #Birnen (z. B. Abate Fetel)
  - 4 EL #Zitronensaft
  - 3 EL #Ahornsirup
  - 100 g Zartbitter (75\%) #Schokolade
  - 160 g #Butter
  - 4 #Eier
  - 90 g #Zucker
  - 1 Pk #Vanillezucker
  - 1 EL #Kakaopulver
  - 3 TL #Backpulver
  - 200 g #Mehl
  - 100 g gehackte #Walnüsse
  - einige Schlücke #Milch
  - Aprikosenkonfitüre zum Bestreichen oder wahlweise Schokoladenkuvertüre
  
Protokoll
---------

  1. Birnenschnitze mit Zitronensaft und Ahornsirup 30 min marinieren
  2. Butter mit Zucker und Vanillezucker cremig rühren und Eier nach und nach zugeben.
  3. Schokolade im Wasserbad schmelzen und etwas abkühlen lassen, zum Rest dazugeben und zu einer cremigen Masse aufschlagen
  4. Mehl, Backpulver, Kakaopulver vermengen und mit etwas Milch abwechselnd in die Schokoladenmasse einrühren.
  5. Am Ende die gehackten Walnüsse und 3 EL der Marinade dazu.
  6. 1/2 des Teigs in Form 10 min backen bei 180 °C
  7. Birnenspalten darauf verteilen mit Rest des Teigs abdecken 30 min backen bei 180 °C.
  8. Den noch warmen Kuchen mit warmer Aprikosenkonfitüre bestreichen.
