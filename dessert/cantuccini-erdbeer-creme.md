# Cantuccini-Erdbeer-Creme

## Zutaten

### Creme

- 250 g #Mascarpone
- 500 g #Magerquark
- 500 g Erdbeeren
- 50 g #Zucker
- 0,5 TL #Zimt
- 1 Becher #Sahne

### Orangen und Schicht

- 4 große #Orangen
- 4 EL #Orangensaft aus 1 Orange
- 4 EL #Zucker 
- 1 TL #Butter 
- 1 Packung #Cantuccini

## Zubereitung

1. 1 Orange auspressen, ergibt ca 125 ml Saft. Davon kommen 100 ml in die Creme, 4 EL mit in die Pfanne zum Karamelisieren
2. Von den restlichen 3 Orangen mit einem Messer Schale und weiße Haut entfernen, sodass nur noch das saftige Innere zu sehen ist. In ca. 0,5 cm dicke Scheiben schneiden
3. 4 EL frisch gepresster Orangensaft zusammen mit 4 EL Zucker in eine Pfanne geben und karamellisieren lassen. Sobald es leicht bräunlich wird, Butter einrühren. Orangen kurz in das Karamell legen und wenden, dann entfernen.
4. Cantuccini klein häckseln, sodass ca. 0,5-1 cm große Stücke übrig sind.
5. Sahne schlagen
6. Für die Creme, 100 ml des frisch gepressten Orangensafts, Zucker, Mascarpone, Quark und Zimt zu einer Creme verrühren. Sahne unterheben.
7. In einer großen Schüssel zwei Schichten legen: Jeweils 1x Cantuccini verteilen, Orangenscheiben auflegen, dann mit Creme glattstreichen. Idealerweise mehrere Stunden im Kühlschrank ziehen lassen.