Partybrot
=========

Ein Laib Brot wird im Schachbrettmuster eingeschnitten, mit Käse und Schinken gefüllt. Man kann dann einzelne Stücke abreißen und essen.

Zutaten
-------

 - 1 Laib #Brot, z. B. Bauernbrot
 - je nach Geschmack #Schinken, #Käse, etc.
 - #Pfeffer, #Salz, #Olivenöl
 
Protokoll
---------

1. Brot mit einem langen Brotmesse längs und quer einschneiden, sodass eine Art Schachbrettmuster entsteht. Nicht bis zum Boden durchschneiden!
2. In die entstandenen Rillen Käse Schinken, etc. füllen
3. Mit Salz und Pfeffer würzen und mit Olivenöl beträufeln
4. 15 min bei 180 °C, bis der Käse geschmolzen ist