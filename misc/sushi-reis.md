# Sushi Reis

 - 200 g #Reis
 - 300 ml leicht gesalzenes Wasser
 - 30 g Essigzubereitung

Reis gut waschen, mit geschlossenem Deckel kurz voll aufkochen, dann auf kleiner Flamme köcheln lassen. Wenn alles Wasser aufgesogen wurde, mit Essig mischen und dann auf Teller verstreichen und abkühlen lassen.

Ergebnis: Reis noch zu hart. Geschmack ok bis fad
