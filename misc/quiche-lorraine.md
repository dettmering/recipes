# Quiche Lorraine

Abgewandelt nach https://www.lidl-kochen.de/rezeptwelt/quiche-lorraine-mit-schwarzwaelder-schinken-und-lauch-147486

## Zutaten

- 3 Stangen #Lauch
- 1 gelbe #Zwiebel 
- 2 EL #Rapsöl 
- 100 g Schwarzwälder #Schinken 
- 100 g #Gouda, jung am Stück
- 250 g saure #Sahne 
- 2 #Eier 
- 1 EL #Butter 
- #Blätterteig
- etwas #Pfeffer 

## Zubereitung

1. Ofen auf 220 °C (Ober-/Unterhitze) vorheizen. Zwiebel halbieren, schälen und in feine Streifen schneiden. Lauch längs halbieren und Wurzelansatz entfernen. Lauchhälften am unteren Ende halten, fächerförmig aufklappen, gründlich waschen und schräg in dünne Streifen schneiden.
2. Eine Pfanne mit Öl auf mittlerer Stufe erhitzen und Zwiebel und Lauch darin unter gelegentlichem Rühren ca. 4 Min. braten. Inzwischen Schinken in Stücke schneiden und Gouda sehr fein würfeln. In einer Schüssel saure Sahne mit Eiern und Gouda verrühren. Etwas Pfeffer hinzugeben.
3. Springform mit Butter einfetten. Teig abrollen, in die Form legen, den Rand andrücken, mit einem Messer begradigen und den Boden mehrfach mit einer Gabel einstechen.
4. Schinken mit der Zwiebel-Lauch-Mischung vermengen und auf dem Quicheteig verteilen. Die Eimasse gleichmäßig darübergeben und die Quiche auf dem Boden des Backofens ca. 25–30 Min. backen.
5. Quiche Lorraine mit Schinken und Lauch aus dem Ofen nehmen und ca. 5 Min. abkühlen lassen. Vorsichtig aus der Form lösen, portionieren und warm servieren.