Pizzateig
=========

Für 4 Pizzen

Zutaten
-------

 - 500g #Weizenmehl 550
 - 280ml Wasser kalt
 - 15g Salz
 - 1-2g #Hefe
 - #Olivenöl

Protokoll
---------

Am besten abends den Teig ansetzen für das Mittagessen am nächsten Tag.

1. Wasser in Schüssel geben, Hefe darin lösen
2. Mehl und Salz dazugeben und in Schüssel gut kneten
3. Teig mit feuchtem Küchentuch abdecken. Nach 30 Minuten erneut falten. Dies 1-2x wiederholen
4. Teig über Nacht stehen lassen
5. 2-3h vor dem Verarbeiten Teig aufteilen und weiter gehen lassen.