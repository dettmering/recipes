Guacamole
=========

Zutaten
-------

 - 5 reife #Avocados
 - 1 kleine #Zwiebeln 
 - 8 #Cocktailtomaten
 - 2 #Knoblauchzehe
 - Saft aus 1 #Zitrone
 - #Pfeffer und #Salz
 
Protokoll
---------

 1. Tomaten und Zwiebel sehr fein hacken, Zitrone auspressen
 2. Avocados schälen, Knoblauch auspressen und in Schüssel zermatschen
 3. Zitronensaft verteilen, Tomaten und Zwiebel dazu geben, gut vermengen
 4. Mit Pfeffer und Salz abschmecken.