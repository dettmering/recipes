Grüner Spargel mit Spinatpesto
==============================

abgewandelt nach https://www.lecker.de/bavette-mit-spargel-und-spinatpesto-69593.html.

Zutaten
-------

- 100-200 g #Spinat 
- 60 g #Parmesan am Stück
- 30 g ganze #Mandeln 
- 1 #Zitrone (Schale essbar)
- 2 #Knoblauch 
- 100 ml #Olivenöl 
- Salz 
- Pfeffer 
- 500 g grüner #Spargel 
- 500 g dünne #Bandnudeln (z. B. Bavette)
- #Pinienkerne

Zubereitung
-----------

1. Spinat waschen, putzen und gut abtropfen lassen. 20 g Parmesan fein reiben. Mandeln grob hacken. Zitrone heiß waschen, abtrocknen und die Schale fein abreiben. Frucht halbieren und auspressen. Knoblauch schälen und grob hacken. 
2. Spinat, Mandeln, Knoblauch und Parmesan in einen hohen Rührbecher geben. Öl zugießen und alles mit dem Stabmixer zu Pesto pürieren. Mit Salz, Pfeffer, Zitronenschale und -saft würzen. 
3. 3–4 l Salzwasser (1 TL Salz pro Liter) aufkochen. Spargel waschen, holzige Enden großzügig abschneiden. Spargel in ca. 3 cm lange Stücke schneiden. Nudeln im kochenden Salzwasser nach Packungsanweisung garen. Spargel ca. 5 Minuten vor Ende der Garzeit zugeben. 
4. Nudeln und Spargel ab­gießen. Sofort mit dem Pesto mischen. Anrichten und mit übrigem Parmesan bestreuen. 