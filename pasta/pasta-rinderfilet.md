Pasta mit Rinderilet
====================

Zutaten
-------

- 500g Spaghetti
- 300g #Rinderfilet
- kleine #Cocktailtomaten
- Bund #Frühlingszwiebeln
- getrocknete #Aprikosen
- #Trüffel
- frischer #Parmesan
- 1 #Knoblauchzehe
- #Sahne

Protokoll
---------

  1. Tomaten halbieren, Frühlingszwiebeln klein hacken, Aprikosen in dünne Scheiben schneiden
  2. Rinderfilet in dünne Stücke schneiden, leicht salzen und pfeffern
  3. Spaghetti kochen
  4. Butter und zerdrückte Knoblauchzehe in Pfanne erhitzen, Rinderfilet scharf anbraten
  5. Tomaten, Frühlingszwiebeln hinzugeben und anschwitzen lassen
  6. etwas Sahne hinzugeben und Aprikosen hinzugeben
  7. 5-10 Min köcheln lassen
  8. Pasta unterheben und mit Parmesan und ggf. Trüffel servieren
