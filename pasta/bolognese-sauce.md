Bolognese Sauce
===============

Wie Vater sie zubereitet. Reicht für 3 mal zu zweit essen.

Zutaten
-------

  - 250g #Hackfleisch vom Schwein
  - 500g Hackfleisch vom Rind
  - 1 #Stangensellerie
  - 2 #Karotten
  - 1 Knoblauchzehe
  - 1 große #Zwiebel
  - neutrales Öl (Raps oder Sonnenblumen, aber kein Olivenöl)
  - 1 400g Dose #Tomaten (keine frischen nehmen)
  - 1 Flasche #Rotwein (mittlere Qualität - keinen superbilligen nehmen)
  - #Gemüsebrühe
  - 1 #Lorbeerblatt
  
Protokoll
---------

  1. Staudensellerie und Karotten putzen und in kleine Stücke schneiden (ca. 1 x 1cm)
  2. Zwiebeln schälen und würfeln
  3. Öl in einem Topf erhitzen (Öl soll im Topf ca. 1cm hoch stehen)
  4. Knoblauchzehe platt drücken und zum Öl geben
  5. Wenn die Zehe anfängt bräunlich zu werden, rausnehmen und wegwerfen
  6. Zwiebeln ins Öl geben, bis sie glasig werden
  7. Karotten und Sellerie dazugeben und ca. 5 Minuten schmoren
  8. Hackfleisch dazugeben und anbraten
  9. Das Ganze mit Rotwein auffüllen, bis das Hackfleisch komplett bedeckt ist (ca. 1cm drüber)
  10. Dosentomaten und Lorbeerblatt dazugeben
  11. Alles gut durchmischen
  12. ca. 2,5h lang auf niedrigster Stufe köcheln
  13. Wenn die Flüssigkeit zu verdampft, zwischendurch immer wieder mit Gemüsebrühe auffüllen
  14. Erst am Schluss salzen, pfeffern und mit etwas Muskatnuss abschmecken.
  