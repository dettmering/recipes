# Pasta mit Lachs in Safran-Zitronensauce

## Zutaten

  - 200g frischer oder tiefgefrorener #Lachs, entgrätet und ohne Haut
  - 400g frische #Pasta wie Tagliatelle oder Spaghetti
  - 1 #Zitrone, Schale essbar
  - 200 ml #Schlagsahne
  - #Weißwein wenn gewünscht
  - 1 TL #Mehl
  - 1/2 #Zwiebel
  - 1 #Knoblauchzehe
  - #Gemüsebrühe
  - #Safran
  - #Olivenöl
  
## Protokoll

  1. Lachs in grobe Würfel schneiden, leicht salzen und mit Zitronensaft beträufeln. Kalt stellen.
  2. Pasta kochen
  3. Zwiebel klein hacken, Knoblauchzehe andrücken
  4. Öl in kleinem Topf erhitzen, Schalotte und Knoblachzehe andünsten, Knoblauchzehe entfernen
  5. Ggf. mit etwas Weißwein ablöschen. Mehl hinzufügen.
  6. 200 ml Wasser und einen halben TL Gemüsebrühe hinzufügen, aufkochen lassen
  7. Sahne unterrühren, Schale der Zitrone hineinreiben, Safran hinzugeben. Köcheln lassen. Mit Pfeffer und Salz würzen.
  8. Währenddessen Öl in großer Pfanne erhitzen, Lachs anbraten. Etwas Zitronensaft dazu.
  9. Soße in Pfanne geben, 5-10 min reduzieren lassen. Abschmecken.
  10. Pasta unterheben.